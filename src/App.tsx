import React, { useState } from 'react';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import './App.css';

//components

import {Search} from './components/search'
import {TxList} from './components/txList'

const theme = createMuiTheme({
  typography: {
    fontSize:11,
  }
});

function App() {

  const [balance, setBalance] = useState();
  const [txs, setTxs] = useState([]);

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <header className="App-header">
        <div className="App-container">
          <Search setTxs={setTxs} setBalance={setBalance} />
          <TxList txs={txs} balance={balance} />
        </div>
      </header>
    </div>
    </ThemeProvider>
    
  );
}

export default App;
