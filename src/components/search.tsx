import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import * as utils from 'web3-utils'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    networkControl: {
      margin: 0,
      minWidth: "100%",
    },
    addressControl: {
      margin: 0,
      minWidth: "100%",
    },
    button:{
      background:"#686B68",
      textAlign:"center",
      padding:"5px"
    }
  }),
);

//API KEY ( SHOULD BE A ENV)
const apiKey = "G9FSUYCGUS4VYFB272R8J6BCEW3ENAQJ3W"
//networks

const rinkeby = {
  balance:`https://api-rinkeby.etherscan.io/api?apikey=${apiKey}&module=account&action=balance&address=`,
  txs:`http://api-rinkeby.etherscan.io/api?apikey=${apiKey}&module=account&action=txlist&page=1&offset=10&sort=desc&address=`,
}
const main = {
  balance:`https://api.etherscan.io/api?apikey=${apiKey}&module=account&action=balance&address=​`,
  txs:`http://api.etherscan.io/api?apikey=${apiKey}&module=account&action=txlist&page=1&offset=10&sort=desc&address=​`
}

export function Search(props) {
  const { setTxs, setBalance } = props
  const classes = useStyles();

  const networksAvailable={"rinkeby":rinkeby,"main":main}

  //validatation
  const [isErrorAddress, setErrorAddress] = useState(false);
  const [isErrorNetwork, setErrorNetwork] = useState(false);
  const [isValid, setIsValid] = useState(true);

 
  //data
  const [recentAddresses, setRecentAddresses] = useState(["0xfffa5813ed9a5db4880d7303db7d0cbe41bc771f"]);
  const [network, setNetwork] = useState("");
  const [address, setAddress] = useState("");

  const handleChangeNetwork = (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
    if (typeof event.target.value == "string"){
      setNetwork(event.target.value);
    }
  };

  const handleChangeAddress = (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
    
    if (typeof event.target.value == "string"){
      setAddress(event.target.value);
    }
  };

  const isNotValidNetwork= () =>{
    return network === ""
  }

  const isNotValidAddress= () =>{
    try {
      utils.toChecksumAddress(address)
      addToRecent()
      return false
    } catch(err){
      return true
    } 
  }

  const addToRecent = ()=>{
    if(recentAddresses.indexOf(address) === -1) {
      recentAddresses.unshift(address)
      recentAddresses.splice(5)
      setRecentAddresses(recentAddresses)
    }
  }

  const handleSubmit = async (e) =>{
    e.preventDefault();

    //if all valid search
    if (!isErrorNetwork && !isErrorAddress){
      await search()
      //add to latest list and cut to latest 5
      addToRecent()
    }
  }

  const getAccountBalance = async ()=>{
    const url = `${networksAvailable[network].balance}${address}`
    const response = await fetch(url)
    const body = await response.json();
    return body.result
  }

  const getAccountTransactions = async ()=>{
    const url = `${networksAvailable[network].txs}${address}`
    const response = await fetch(url)
    const body = await response.json();
    return body.result
  }

  const search = async ()=> {
    const [addressBalance, addressTxs] = await Promise.all([
      getAccountBalance(),
      getAccountTransactions(),
    ])
    setBalance(addressBalance)
    setTxs(addressTxs)
  }

  useEffect(() => {
    //validate address
    setErrorAddress(isNotValidAddress())
  }, [address]);

  useEffect(() => {
    //validate network
    setErrorNetwork(isNotValidNetwork())
  }, [network]);

  return (
    <div>
      <form onSubmit={handleSubmit}>
      <div>
        <FormControl variant="filled" error={isErrorNetwork} className={classes.networkControl}>
        <InputLabel id="net-select-label">Network</InputLabel>
        <Select
          labelId="net-select-label"
          id="net-select"
          value={network}
          onChange={handleChangeNetwork}
        >
          {Object.keys(networksAvailable).map((n)=>{
            return <MenuItem key={n} value={n}>{n}</MenuItem>
          })}
        </Select>
      </FormControl></div>
      
      <FormControl variant="filled" className={classes.addressControl}>
        <InputLabel id="address-select-label">Address Book</InputLabel>
        <Select
          labelId="address-select-label"
          id="address-select"
          value={address}
          onChange={handleChangeAddress}
        >
          {recentAddresses.map((n)=>{
            return <MenuItem key={n} value={n}>{n}</MenuItem>
          })}
        </Select>
      </FormControl>
      <TextField  className={classes.addressControl} error={isErrorAddress} id="address-basic" label="Address" variant="filled" value={address} onChange={handleChangeAddress} />
      <div className={classes.button}>
        <Button type="submit">Search</Button> 
      </div>
      </form>
    </div>
  );
}
