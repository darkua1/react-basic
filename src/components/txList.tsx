import React, { useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import {TxItem} from './txItem' 
import * as utils from 'web3-utils'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      padding:"5px"
    },
  }),
);

export function TxList(props) {
  const classes = useStyles();
  const {txs, balance} = props
  
  return (
    <div className={classes.list}>
      {balance && <div>Balance : {utils.fromWei(utils.toBN(balance))}ETH</div>}
      {txs.length !== 0 && <div>
      <p> Latest 10 txs:</p>
        {txs.map((tx)=>{
          return <TxItem key={tx.hash} tx={tx}/>
        })}
      </div>}
      
    </div>
  );
}
