import React, { useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import * as utils from 'web3-utils'
import Modal from '@material-ui/core/Modal';
import Link from '@material-ui/core/Link';
import * as QRCode from 'qrcode.react';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    item: {
      width:"100%",
      opacity:"0.5",
      fontSize:"0.8em",
      textAlign:"left",
      marginBottom:"10px"
    },
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      border:"0",
      background:"0xFFFFFF"
    },
  }),
);

export function TxItem(props) {
  const classes = useStyles();
  const {tx} = props
  
  const [open, setOpen] = React.useState(false);
  const [account, setAccount] = React.useState("");

  const handleOpen = (account) => {
    setAccount(account)
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const body = (
    <div className={classes.paper}>
      <QRCode value={account} />,
    </div>
  );

  return (
    
    <div className={classes.item}>
      <div>
        <Link href="#" onClick={()=>handleOpen(tx.to)}>{tx.to}</Link>
      </div>
      <div>{utils.fromWei(tx.value)}ETH</div>
      <Modal
        open={open}
        className={classes.modal}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  )
}
